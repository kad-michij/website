---
title: Contributie
weight: 10
description: >
  Voel jij je aangesproken en wil je meteen al actief bijdragen? Hier lees je hoe dat kan
---

<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/welkom/">
  Welkom <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/welkom/werkomgeving/">
  Werkomgeving <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-secondary me-3 mb-4" href="https://digilab.overheid.nl/chat" target="_blank">
  Mattermost <i class="fas fa-comments ms-2"></i>
</a>
<a class="btn btn-lg btn-secondary me-3 mb-4" href="https://gitlab.com/datastelsel.nl/federatief/website" target="_blank">
  Sources <i class="fab fa-gitlab ms-2 "></i>
</a>

Bovenstaande links zijn goede startpunten, afhankelijk van wat je zoekt en/of nodig hebt.
