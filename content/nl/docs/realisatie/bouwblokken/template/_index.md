---
title: Template bouwblok
description: >
  Template voor een bouwblok.
---

## Beschrijving

## Architectuurlagen

### Grondslagenlaag

Hier vind je wetten en drivers.

Componenttypen die we hier verwachten:

- Doel/Driver
- Principe
- Requirement
- Afspraak/Standaard (Wet/Overeenkomst)

### Organisatorische laag

De organisatorische componenten vind je hier.

Componenttypen die we hier verwachten:

- (Actor)
- Rol
- Functie
- Service
- Afspraak/Standaard
- (Contract)

### Informatielaag

Componenttypen die we hier verwachten:

- Metadata-object
- Metadata-object-relatie
- Afspraak/Standaard

### Applicatielaag

Componenttypen die we hier verwachten:

- Functie (Technische stelselfuncties)
- Applicatieservice
- Afspraak/Standaard

### Technologielaag

Componenttypen die we hier verwachten:

- Node
- Netwerk
- Afspraak/Standaard
