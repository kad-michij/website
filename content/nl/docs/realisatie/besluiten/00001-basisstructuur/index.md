---
title: 00001 Basisstructuur
date: 2023-10-01
type: besluiten
categories: [DR]
tags: 
status: draft
description: >
   We gebruiken de Soft Infrastructure Building Blocks van OpenDEI als basis voor onze structuur
---

## Context en probleemstelling

Als Nederlands federatief datastelsel hebben we de [strategie van
samenwerken](/docs/welkom/strategie-van-samenwerken/). Dat betekent dat we verbinding nodig hebben
en moeten ondersteunen. Het helpt als daar een herkenbare en eenvoudige structuur aan ten grondslag
ligt. Dit wordt ook wel 'structure follow strategy' genoemd. Er zijn echter vele architecturen,
architectuur frameworks en ... fêtes rondom architecturen. Wat is wijs? Wat is herkenbaar? Wat zou
grote verschillen kunnen overbruggen? En hoe blijven we buiten of ontwijken we 'architectuur fêtes'?

## Beslissingsfactoren <!-- optional -->

- Omliggende architecturen
  - [NORA](https://www.noraonline.nl/wiki/NORA_online)
  - [Common Ground Architectuur](https://commonground.nl/cms/view/272a6103-4c0a-46c3-a597-5d7199aca983/architectuur)
- Context:
  - Europese Unie en ontwikkelingen
  - Doelstellingen [Regie op
    Gegevens](https://www.digitaleoverheid.nl/overzicht-van-alle-onderwerpen/regie-op-gegevens/)
  - Doelstellingen [Realisatie Interbestuurlijke Datastrategie
    (IBDS)](https://realisatieibds.pleio.nl/)
  - Doelstellingen [Federatief Datastelsel
    (FDS)](https://realisatieibds.pleio.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
  - Doelstellingen [Common Ground](https://commonground.nl/)
  - Doelstellingen [Data bij de Bron](https://www.digitaleoverheid.nl/data-bij-de-bron/)
  - Doelstellingen [Digilab](https://digilab.overheid.nl/)
- Uitgangspunten:
  - Europa volgen
  - Capabilities
  - Architectuur Building Blocks
  - NORA 5 lagen
  - Archimate lagen
  - Basis voor samenwerking

## Overwogen opties

- [Design Principles for Data Spaces by OpenDEI](#design-principles-for-data-spaces-by-opendei)
- [NORA Vijflaagsmodel](#nora-vijflaagsmodel)
- [Archimate](#archimate)

## Besluit

We kiezen voor een [Capabilities](/docs/realisatie/capabilities/) model gebaseerd op het OpenDEI
building blocks model gecombineerd met een vijf lagen model gebaseerd op de NORA vijf lagen model en
Archimate. We kiezen voor een Nederlandse vertaling en eigen bewoordingen. Tegelijk zorgen we ervoor
dat de verbinding met de oorspronkelijke modellen heel zichtbaar en herkenbaar blijft.

![Basisstructuur / Raamwerk](images/raamwerk.jpg)

Het raamwerk / de basisstructuur wordt in het [raamwerk voor ontwikkeling in
samenwerking](/docs/realisatie/raamwerk/) verder uitgebreid
beschreven.

De basisstructuur is de 'kapstok' om alle ontwikkelingen en aspecten van het federatief datastelsel aan 'op te hangen'. Het biedt een 'landkaart' om aan te geven waar een bepaalde functionaliteit, functie, techniek zich bevindt. De verbinding naar omliggende zaken is als volgt te zien:

![Basisstructuur als kapstok](images/raamwerk-volledige-context.jpg)

### Positieve gevolgen <!-- optional -->

- De OpenDEI building blocks, bij ons capabilities genoemd, geven herkenbaarheid en verbinding naar
  Europese ontwikkelingen en initiatieven.
- De capabilities bieden ook verbinding naar capabilities in andere Nederlandse initiatieven,
  aangezien ook zij 'capabilities' gebruiken. Hoe de verschillende capabilities zich tot elkaar
  verhouden kan nog wel verschillen en/of uitzoekwerk betekenen. Wij denken dat de OpenDEI building
  blocks bruikbare capabilities zijn.
- De vijf lagen van ons model zijn duidelijk te verbinden met de vijf lagen van NORA en Archimate.
  Daarmee worden veel verbindingen duidelijk en eenvoudig.
- Verbindingen zijn met deze basisstructuur eenvoudig, duidelijk en herkenbaar. Dat geeft de
  mogelijkheid om met vele initiatieven om het federatief datastelsel heen te verbinden. Dat sluit
  aan bij onze [Strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/)

### Negatieve Consequences <!-- optional -->

- We introduceren een nieuw raamwerk (framework) en dat zal daarom op moment van introductie
  _nergens_ helemaal mee aansluiten. Dit vergt tijd en werk om verbindingen te maken en per
  verbinding te onderzoeken en aan te geven hoe relaties precies gemaakt kunnen worden. (We denken
  wel dat ons model zo herkenbaar en eenvoudig is, dat dit niet echt tot problemen zal leiden)

## Pros en Cons van de oplossingen <!-- optional -->

### Design Principles for Data Spaces by OpenDEI

De [Position Paper: Design Principles for Data
Spaces](https://design-principles-for-data-spaces.org/) by the OpenDEI Workforce heeft al veel
voorwerk en uitzoekwerk gedaan over datastelsels, oftewel Data Spaces. Dit raamwerk is dan ook al
veel gebruikt voor Europees gerelateerde ontwikkelingen en initiatieven. Ook de samenwerking en
vergelijk met IDSA en GAIA-X worden hierin benoemd.

Voor het federatief datastelsel is de vergelijking door Geonovum van groot belang en ook hier worden
de 'building blocks' van OpenDEI gebruikt. In de [Geonovum Verkenning
Dataspaces](https://docs.geostandaarden.nl/eu/VerkenningDataspaces/) wordt het OpenDEI model als
'conceptueel model' gebruikt voor de 'quick-scan (zie
[H2.4](https://docs.geostandaarden.nl/eu/VerkenningDataspaces/#4592FE93)).

![OpenDEI building blocks](images/opendei-soft-infrastructure-capabilities.png)

Voor de vergelijking van het federatief datastelsel helpt dit model vooral in de verbinding naar
Europa en Europese ontwikkelingen en initiatieven. Aangezien dit een belangrijk gebied is voor het
federatief datastelsel, is ook dit moment belangrijk.

### NORA Vijflaagsmodel

In de NORA, de Nederlandse Overheid Referentie Architectuur, wordt het
[Vijflaagsmodel](https://www.noraonline.nl/wiki/Vijflaagsmodel) beschreven. Dit is de basis van veel
overheidsarchitecturen en daarom erg herkenbaar. Tegelijk is het afwijkend van
[Archimate](#archimate), welke ook veel gebruikt wordt (ook buiten de overheid).

![NORA Vijflaagsmodel](images/Vijf_lagen_bewerkt.png)

### Archimate

Archimate is een architectuur taal. Een visuele taal met een rijke set aan gegronde en doordachte
mogelijkheden. Dit is een veelgebruikte basis voor architecturen, ook als deze 'niet-Archimate'
heten. De rijkheid van Archimate en volledigheid biedt veel mogelijkheden. Tegelijk is dat wellicht
de zwakte; er is zoveel mogelijk ... dat overzicht verloren gaat aan eigen succes.

Om vanuit het federatief datastelsel wel gemakkelijk verbinding te maken vanuit architecturen die (direct of indirect) gebaseerd zijn op Archimate, hebben wij in de basisstructuur daar duidelijk verbinding en overeenkomst gemaakt in de lagen.

![Archimate 3.1 Full Framework](images/ArchiMate-3.1-Full-Framework.jpg)

### Capabilities

Veel architecturen kennen het woord 'capability', 'bekwaamheid', iets dat 'het moet kunnen'. Dit
komt voor in zowel
[TOGAF](https://pubs.opengroup.org/togaf-standard/business-architecture/business-capabilities.html)
als in de [NORA](https://www.noraonline.nl/wiki/Capability).

Daarnaast wordt bouwblok (building block) veel gebruikt in architecturen. Soms zijn het synoniemen,
soms duidt het op verschillende zaken. Dit is (helaas) niet altijd duidelijk. Dat komt ook doordat
het in verschillende architectuur frameworks verschillend gebruikt wordt.

In de basisstructuur / raamwork wordt 'capability' vooral op de bovenste laag van het vijflagen
model gezien. Dit komt overeen met hoe capabilities in [Archimate](#archimate) gebruikt worden.
 

## Links

- doc [Strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/)
- doc [Raamwerk (voor ontwikkeling in samenwerking)](/docs/realisatie/raamwerk/)
- doc [Werkomgeving](/docs/welkom/werkomgeving/)
- DR [00003 Werkomgeving voor ontwikkeling](/docs/realisatie/besluiten/00003-werkomgeving-voor-ontwikkeling/)

