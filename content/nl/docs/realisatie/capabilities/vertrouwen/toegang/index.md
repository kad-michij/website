---
title: Toegang
weight: 2
date: 2023-10-17
categories: [Capabilities, Vertrouwen, Toegang]
tags: 
description: >
  Vertrouwen | Toegang: Toegangscontrole en gebruikscontrole
---

{{< capabilities-diagram selected="toegang" >}}

**Vertrouwen | Toegang**:

Dit bouwblok garandeert de handhaving van het beleid inzake gegevenstoegang en -gebruik dat is
gedefinieerd als onderdeel van de algemene voorwaarden die zijn vastgesteld wanneer gegevensbronnen
of -diensten worden gepubliceerd (zie de bouwsteen 'Publicatie- en dienstenmarktplaats' hieronder)
of waarover tussen providers en consumenten wordt onderhandeld. Een data-aanbieder implementeert
doorgaans mechanismen voor data-toegangscontrole om misbruik van bronnen te voorkomen, terwijl
controlemechanismen voor data-gebruik doorgaans worden geïmplementeerd aan de kant van de
data-afnemer om misbruik van gegevens te voorkomen. In complexe datawaardeketens worden beide
mechanismen door prosumenten gecombineerd. Toegangscontrole en gebruikscontrole zijn afhankelijk van
identificatie en authenticatie (zie bouwblok [Vertrouwen | Identiteitsbeheer](identiteitsbeheer)).
