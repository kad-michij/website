---
title: Verantwoording
weight: 2
date: 2023-10-17
categories: [Capabilities, Datawaarde, Verantwoording]
tags: []
description: >
  Datawaarde | Verantwoording : Verantwoording van datagebruik
---

{{< capabilities-diagram selected="verantwoording" >}}

**Datawaarde | Verantwoording**:

De verantwoording tot en/of gebruik van gegevens door verschillende gebruikers. Dit ondersteunt op
zijn beurt belangrijke functies voor clearing, betaling en facturering (inclusief transacties voor
het delen van gegevens zonder tussenkomst van datamarktplaatsen).
