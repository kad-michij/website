---
title: Metadata
weight: 1
date: 2023-10-17
categories: [Capabilities, Datawaarde, Metadata]
tags: 
description: >
  Datawaarde | Metadata: Metadata in brede zin met beschrijvingen, verwijzingen en meer
---

{{< capabilities-diagram selected="metadata" >}}

**Datawaarde | Metadata**:

Metadata in brede zin met beschrijvingen, verwijzingen en meer. Ook de discovery,
ontdekkingsmechanismen, van metadata valt onder deze capability, hoewel daar ook een sterke
afhankelijkheid met [Datawaarde | Publicatie](../publicatie/) in zit. Voor deze beschrijvingen en
verwijzingen wordt gebruik gemaakt van gemeenschappelijke (zelf)beschrijvingen van bronnen, diensten
en deelnemers. Dergelijke beschrijvingen kunnen zowel domein-agnostisch als domein-specifiek zijn.
Ze moeten mogelijk worden gemaakt door semantische webtechnologieën en moeten principes van
gekoppelde data omvatten.
