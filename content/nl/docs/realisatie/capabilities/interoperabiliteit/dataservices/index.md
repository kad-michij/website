---
title: Dataservices
weight: 2
date: 2023-10-17
categories: [Capabilities, Interoperabiliteit, Dataservices]
tags: 
description: >
  Interoperabiliteit | Dataservices: API's voor data uitwisseling
---

{{< capabilities-diagram selected="dataservices" >}}

**Interoperabiliteit | Dataservices**:

API's voor data uitwisseling ...

Deze bouwsteen vergemakkelijkt het delen en uitwisselen van gegevens door participanten van de dataruimte. 

## Bouwblokken

- [Dataservice](/docs/realisatie/bouwblokken/uitwisseling-apis/)
- [Self-description](/docs/realisatie/bouwblokken/self-description/)
