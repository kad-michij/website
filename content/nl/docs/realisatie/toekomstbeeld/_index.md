---
title: Toekomstbeeld
weight: 10
description: >
  Het toekomstbeeld van het federatief datastelsel, wat we nu al weten,
  onderdelen die daar aan bij gaan dragen, principes.
---

{{% pageinfo %}}

Hier gaat het toekomstbeeld dat de basis is van [Realisatie IBDS](https://realisatieibds.pleio.nl)
worden gepubliceerd.

Hieronder volgen verdiepingen / uitwerkingen voor diverse onderwerpen, ook wel 'onze 2-pagers'
genoemd.

{{% /pageinfo %}}

