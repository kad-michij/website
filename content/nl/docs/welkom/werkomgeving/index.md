---
title: Werkomgeving
weight: 20
date: 2023-10-23
author: Marc van Andel
tags: [Realisatie IBDS]
description: >
  Wij werken open; zie onze [strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/).
  Maar hoe dan? Vooral door samenwerking in communities te faciliteren.
---

Zoals beschreven in onze [strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/) werken
wij **open** en **community gedreven**. Daarom faciliteren wij in een omgeving waarin open
samenwerking in kan floreren. Die omgeving beslaat een aantal aspecten van communicatie van een
community: 

<img src="images/community-tools-simple.jpg" alt="Community Tools simple diagram" width="300"
align="right">


- Publicatie en landingsplaats
- Ontwikkeling van documenten (en code)
- Interactie, zoals chat en online communicatie
- Issue tracking; het volgen en communicatie over issues


## Community

Een community is ... voor ons elke groepsvorm. Mensen werken samen in enige vorm, vanuit enig
gedeelde achtergrond of driver. Graag willen we communities ondersteunen, verwelkomen en zo de
samenwerking bevorderen en faciliteren. En toch ... zijn er dan spelregels nodig.

We laten ons inspireren door 'open source communities'. Hierin is het gewoon om een kern van 'core
committers' te hebben. Zij zijn de 'guardians', de beschermers, de hoeders van de community. Zo
geldt dat ook voor de community rondom het federatief datastelsel. In beginsel is het 'FDS Team'
deze groep van 'core committers'. Bij de groei van de community en de ontwikkeling van het
federatief datastelsel zijn veranderingen zeker mogelijk. Maar we moeten ergens beginnen.

Om de kern bevindt zich een grotere groep van 'contributors'. Dit zijn mensen die vanuit
verschillende expertise, achtergronden, teams en _whatever_ bijdragen doen aan 'het open source
project'; in dit geval het federatief datastelsel. Dit is iedereen die een bijdrage in enige vorm
doet. Dat kan door opmerkingen te maken, heuse bijdragen in Merge Requests (of Pull Requests) aan te
leveren, vragen te stellen, antwoorden te geven, actief betrokken in discussies en communicatie. Dit
is open voor iedereen om in te stappen. Ook hiervoor gelden er regels om ordelijk en goed met elkaar
om te gaan en inclusief te zijn. Zie hiervoor onze [Contribution
Guidelines](/docs/community/contribution/).

Iedereen die meeleest, volgt en gebruik maakt van de kennis en functionaliteiten van het federatief
datastelsel, kan gezien worden als 'gebruiker'. Dit is de grote groep mensen, geïnteresseerden en
passief betrokken mensen. Uiteindelijk doen we het daarvoor. Als we van toegevoegde waarde kunnen
zijn voor onze gebruikers, dan gaat het goed. Gebruikers is hier wel een enigszins ongelukkige term,
want uiteindelijk gaat het natuurlijk om het ondersteunen van maatschappelijke vraagstukken en het
delen van de data die daarvoor nodig is. Gebruikers zijn dan de mensen die hierbij helpen, gebruik
van maken of in enige vorm geraakt worden door wat er met het federatief datastelsel mogelijk wordt
of daar ontwikkeld wordt.

## Publicatie en ontwikkeling documenten

Voor de publicatie en landingsplaats voor iedereen, zowel intern als extern, gebruiken wij Pleio:
[realisatieibds.pleio.nl](https://realisatieibds.pleio.nl/). Dit is de formele publicatie en
communicatie kanaal.

Toch is alleen deze Pleio site onvoldoende. Of beter gezegd: het versiebeheer en samenwerken is
onvoldoende. Daarvoor kiezen we graag voor _**best of breed**_ tools. Aangezien [Git](#git) een
superieur en de facto standaard versiebeheersysteem is, kiezen we voor een open omgeving om daarmee
te kunnen werken. GitLab is hier de 'tool of choice'. Dat betekent dat de ontwikkeling en
samenwerking op documenten in Git gebeurt. Daarvoor is een eenvoudig formaat nodig waarin de inhoud
(content) van de documenten wordt ontwikkeld en van waaruit nette publicatie site gegenereerd kan
worden. Daarom passen we daar [Markdown](#markdown) toe. Dit is een eenvoudig formaat van tekst in
tekstbestanden, wat voor Git ideaal is, en ook machine-leesbaar is. De in de tekst toegevoegde
opmaaktekens worden omgezet naar visuele opmaak bij het produceren van een website. Dezelfde tekst
(bron) kan gebruikt worden om ook andere formaten zoals PDF te produceren.

## Git

Wat is Git? Git is een versiebeheersysteem (Version Control System). Maar wat is dat precies?

Stel: Je werkt aan een set van documenten. Deze documenten heb je in een mapje (directory, folder)
op je laptop staan. Op een gegeven moment heb je een versie die klopt. De samenhang is goed. De
verwijzingen onderling zijn goed. Je wilt die versie wel even bewaren voordat je er in verder gaat
werken. Daarom maak je een _kopie_ van de collectie van bestanden. Het gemakkelijkste daarvoor is
een kopie van de map te maken waar de hele collectie van bestanden in zit. Vervolgens werk je verder
en af en toe vergelijk je de nieuwe versie met de vorige versie. Of zelfs vorige versies ...

Dit is precies de functionaliteit die Git biedt!! Zo'n kopie bewaren heet een **git commit**. Je
'commit' al je wijzigingen die je op dat moment hebt opgespaard. Vervolgens biedt Git je vele
mogelijkheden om te vergelijken (_diff_), samen te voegen (_merge_), met verschillende kopieën naast
elkaar door te werken (_branch_), etc, etc. Er zijn zelfs hele workflows omheen ontstaan, die helpen
om bijdragen op een goede manier te reviewen, te accepteren en samen te voegen in de volgende versie
van de verzameling documenten (of code). Dat komt omdat Git zeer uitgebreid is in het werken met
**branches**. Zo'n branch is een parallelle kopie waarin wijzigingen als commits kunnen worden
gedaan en bewaard.

Een branching workflow met Git ziet er dan als volgt uit:

![Git branching](images/git-branching.jpg)

## GitLab

Een verzameling bestanden, documenten (of broncode) wordt in Git beheerd in een Git **repository**,
ook wel afgekort naar _Git repo_. Git biedt uitgebreide mogelijkheden om repositories decentraal te
beheren en om vervolgens wijzigingen (commits) uit te wisselen. Er is technisch geen centrale opslag
of server nodig ... maar voor de samenwerking met mensen is dat wél nodig 😄 Dit wordt vaak de _Git
hub_ genoemd; de hub tussen alle decentraal beheerde repo's (repositories). Wij kiezen voor
**GitLab** als de 'Git hub' / centrale Git server. _(Bekend alternatief is GitHub en aangezien men
hier wellicht mee bekend is, wordt in dit artikel bij sommige onderdelen de GitHub terminologie ook
benoemd.)_

GitLab (en vergelijkbare producten als GitHub) bieden allerlei ondersteuning via het web. Zoals
gezegd heeft Git sublieme mogelijkheden om in _branches_ parallel te kunnen werken. Parallel
betekent in deze dat mensen parallel kunnen werken, maar ook dat versies of documenten met
verschillende aandachtsgebieden parallel kunnen worden ontwikkeld. GitLab biedt dan vervolgens
ondersteuning in het review proces en de _merge_ van zo'n parallelle branch in het 'hoofd product',
de `main` branch. Dit heet (in GitLab) een **Merge Request** (in GitHub: _Pull Request_). Zo'n Merge
Request geeft een goed inzicht in wat er veranderd is, wat er toegevoegd wordt, wat er verwijderd
wordt. Het biedt ook mogelijkheden om commentaar te geven in het review proces. En dit alles
inzichtelijk en gedetailleerd. Het niveau van detail kan zelfs wel overweldigend ervaren worden 😉

<a class="btn btn-lg btn-secondary me-3 mb-4" href="https://gitlab.com/datastelsel.nl/federatief/website" target="_blank">
  Sources <i class="fab fa-gitlab ms-2 "></i>
</a>

## Markdown

Markdown is een veelgebruikte standaard en misschien wel de facto standaard voor de ontwikkeling van
documenten. Aangezien wij deze breed willen inzetten, is de beschrijving en toelichting op de opmaak
opgenomen in onze help: [Docs / Community / Hulp / Markdown](/docs/community/hulp/markdown/).

## Links

- doc [Strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/)
- doc [Raamwerk voor ontwikkeling in
  samenwerking](/docs/realisatie/raamwerk/)
- DR [00001 Basisstructuur](/docs/realisatie/besluiten/00001-basisstructuur/)
- DR [00003 Werkomgeving voor ontwikkeling](/docs/realisatie/besluiten/00003-werkomgeving-voor-ontwikkeling/)
- [Docs / Community / Contribution Guidelines](/docs/community/contribution/)
- [Docs / Community / Hulp / Markdown](/docs/community/hulp/markdown/)
