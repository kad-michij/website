---
title: Strategie van samenwerken
weight: 10
date: 2023-10-18
author: Marc van Andel
tags: [Realisatie IBDS]
description: >
  Een Nederlandse datastrategie kan alleen met elkaar gerealiseerd worden en daarom is er
  interbestuurlijk **strategie van samenwerken** nodig.
---

Van woningtekort en energietransitie tot armoede, schulden en zorg. Om maatschappelijke vraagstukken
aan te pakken, is data binnen de overheid onmisbaar. Voor beleid, uitvoering, toezicht en
verantwoording. De Interbestuurlijke Datastrategie (IBDS) is opgezet om de kansen van verantwoord
datagebruik te benutten en om de knelpunten aan te pakken. Wat mag? Wat kan? Wat helpt? Wat
inspireert?

De IBDS is het resultaat van nauwe samenwerking tussen departementen, uitvoeringsorganisaties en
koepels van gemeenten, provincies en waterschappen. De IBDS is op [18 november 2021 naar de Tweede
Kamer
verzonden](https://www.rijksoverheid.nl/documenten/kamerstukken/2021/11/18/kamerbrief-over-interbestuurlijke-datastrategie-nederland).
Het streeft naar de verantwoorde inzet van data voor maatschappelijke opgaven en zet een ambitieuze
stip op de horizon. Daarmee past het bij de doelen van het programma Werk aan Uitvoering (WaU) en de
werkagenda Waardengedreven Digitaliseren. 

Om de ambities uit de IBDS te realiseren, is in 2022 gestart met het programma Realisatie IBDS. In
2023 is interbestuurlijk de
[Meerjarenaanpak](https://realisatieibds.pleio.nl/news/view/a8f47553-c0e1-475d-a294-9cd5d24a6b30/meerjarenaanpak-ibds-bekrachtigd)
opgesteld.

## Aanpak

De IBDS werkt vanuit de praktijk. Het programma jaagt de ontwikkelingen aan, vanuit verbinding en
samenwerking, en door kennis en ervaringen met elkaar delen. Hiervoor brengt het programma over
ketens heen partijen bij elkaar, die samen werken aan een vraagstuk. Dat is ook nodig om als
overheid echt goed met data te kunnen werken. Bij
[vraagstukken](https://realisatieibds.pleio.nl/cms/edit/0f8c8472-1829-48ef-82f9-afe6d2729f27) rondom
het woningtekort of de energietransitie zitten de databronnen immers nooit bij één partij. 

Realisatie IBDS neemt hier het initiatief door **strategie van samenwerken**. Wij werken open en
zijn toegankelijk voor samenwerking en hulp. Wij faciliteren het gesprek, de verbinding, de
samenwerking. Tegelijk moeten er keuzes gemaakt worden. Wij ondersteunen de discussies en het proces
om te komen tot echte interbestuurlijke besluiten.

&nbsp;  

![het federatief datastelsel maken we
samen](images/het-federatief-datastelsel-maken-we-samen-sticker.png)

## Uitwerking

Het strategie van samenwerken werken wij concreet uit in een aantal punten.

### Wij werken open

Om te kunnen samenwerken is het noodzakelijk dat we transparant zijn, open zijn in wat we doen. Onze
intenties, onze ideeën, onze ontwikkeling is open te volgen. Al onze documenten zijn in principe
openbaar. (Uitzonderingen betreffen vooral interne bedrijfsvoering en bescherming van
persoonsgegevens)

Daarnaast hebben wij een open houding en zijn wij toegankelijk. Wij zijn bereikbaar, aanspreekbaar,
zichtbaar.

### Wij werken community gedreven

Samenwerking over organisatiegrenzen heen is per definitie een vorm van werkgroepen,
samenwerkingsverbanden, van communities. Wij werken actief aan de ontwikkeling van communities. We
bouwen een eigen netwerk van communities door bestaande communities te verbinden en te ondersteunen.

Communities zijn gebaat bij goede communicatie. We werken actief aan goede ondersteuning van
communicatie. Dit richt zich op vier onderdelen:

- Publicatie en 'landingsplaats'; dit is het startpunt voor alle communicatie
- Documenten, publiek en met goed versiebeheer ondersteunt
- Chat en directe communicatie kanalen
- Issue tracking, beheer van vragen en opvolging

### Wij verbinden

Er is al heel veel beschikbaar. Er zijn programma's, ontwikkelingen, doorontwikkelingen die al jaren
onderweg zijn. Er zijn 'good practices' die zich al bewezen hebben. Wij maken graag gebruik van wat
er al is, van wat zich al bewezen heeft.

Wij faciliteren daarom inzicht en overzicht in programma's, ontwikkelingen van organisaties en
verbinden actief op thema's. Uiteraard beperken we ons hierin voor zaken die gericht zijn op en
verband houden met het federatief datastelsel.

Open werken is in de open source 'wereld' al lang in ontwikkeling. Daar is werken in communities ook
bekend en beproefd. Voor het actief open werken kunnen we daar van leren en overnemen wat daar goed
werkt.

### Wij nemen faciliterend initiatief

Wij nemen initiatief op bovenstaande zaken. Op deze manier faciliteren wij het gesprek, discussies
en onderwerpen. Door het nemen van het initiatief hierin laten we leiderschap zien. Wij zijn gericht
op het ontwikkelen van vertrouwen van en in de communities.

Het vertrouwen is belangrijk en van belang. Uiteindelijk staan we voor moeilijke keuzes. Als
maatschappij en overheden gaan we veranderingen door moeten maken. Dat vraagt om het nemen van
besluiten. Wij zijn faciliterend in het gesprek en de discussies en tegelijk gericht op het nemen
van besluiten. Wij zijn daarom actief gericht op het tot besluiten brengen van discussies. Ten
overvloede, dit willen in en met vertrouwen doen in het netwerk van communities, waarin belangen
juist en secuur worden afgewogen. Hierin komt het Waardengedreven Digitaliseren tot uiting.

## Links

- doc [Raamwerk (voor ontwikkeling in samenwerking)](/docs/realisatie/raamwerk/)
- doc [Werkomgeving](/docs/welkom/werkomgeving/)
- DR [00001 Basisstructuur](/docs/realisatie/besluiten/00001-basisstructuur/)
- DR [00003 Werkomgeving voor
  ontwikkeling](/docs/realisatie/besluiten/00003-werkomgeving-voor-ontwikkeling/)

&nbsp;  

---

Ter inspiratie links over diendend leiderschap:

- <a href="https://www.talentontwikkeling.com/blog/dienend-leiderschap-rollen/" target="_blank">Talentontwikkeling | Betere teamprestaties dankzij dienend leiderschap</a>
- <a href="https://www.pwnet.nl/28348/wat-is-dienend-leiderschap" target="_blank">pw. | Wat is dienend leiderschap?</a>
- <a href="https://psychologievansucces.nl/de-essentie-van-dienend-leiderschap-met-inge-nuijten/" target="_blank">Psychologie van Succes | De essentie van Dienend Leiderschap (met Inge
Nuijten)</a>
