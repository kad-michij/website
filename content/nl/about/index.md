---
title: federatief.datastelsel.nl
linkTitle: Over
menu: {main: {weight: 10}}
---

{{% blocks/cover title="federatief.datastelsel.nl" image_anchor="bottom" height="auto" %}}

Gedreven vanuit de overheid en de noodzaak aan data voor maatschappelijke vraagstukken, wordt er
hard gewerkt aan de **ontwikkeling** van het **federatief datastelsel** van **Nederland**!

Op dit moment ligt het initiatief en het samenkomen van andere initiatieven bij het programma
onderdeel [Federatief
Datastelsel](https://realisatieibds.pleio.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
(FDS) van het programma [Realisatie IBDS](https://realisatieibds.pleio.nl/). IBDS staat voor
Interbestuurlijke Datastrategie.

{{% /blocks/cover %}}

{{% blocks/section %}}

Wat is het programma IBDS? De IBDS is het resultaat van nauwe samenwerking tussen departementen,
uitvoeringsorganisaties en koepels van gemeenten, provincies en waterschappen. De IBDS is op 18
november 2021 naar de Tweede Kamer verzonden en streeft naar de verantwoorde inzet van data voor
maatschappelijke opgaven. Daarmee past het bij de doelen van het programma Werk aan Uitvoering (WaU)
en de werkagenda Waardengedreven Digitaliseren.

De ontwikkeling van het federatief datastelsel is een van de pijlers binnen dit programma, maar het
programma doet nog veel meer. Op [Pleio](https://realisatieibds.pleio.nl/) kun je alles vinden over
de pijlers. Het team Federatief Datastelsel (FDS) is verantwoordelijk voor het realiseren van dit
datastelsel voor de toekomst, ook nadat het programma is afgerond. Op dit platform bouwen we aan dit
langer termijn product en kent daardoor een ander karakter dan alleen de programmaduur.
 
Het federatief datastelsel kun je zien als het data-ecosysteem van de Nederlandse overheid; een
vertrouwensraamwerk voor cross-sectorale datadeling tussen organisaties met wettelijke taken. Het
raamwerk bestaat uit functie, afspraken, standaarden en voorzieningen.

{{% /blocks/section %}}

