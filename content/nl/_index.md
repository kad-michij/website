---
title: federatief.datastelsel.nl
---

{{< blocks/cover title="Welkom bij het federatief datastelsel van Nederland!" image_anchor="top" height="full" >}}
<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/">
  Documentatie <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-primary me-3 mb-4" href="/community/">
  Community <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-info me-3 mb-4" href="https://realisatieibds.pleio.nl/" target="_blank">
  Interbestuurlijke Datastrategie <i class="fa fa-globe ms-2 "></i>
</a>
<p class="lead mt-5">Hier bouwen we samen aan het federatief datastelsel!</p>
{{< blocks/link-down color="info" >}}
{{< /blocks/cover >}}


{{% blocks/lead color="primary" %}} 

De **ontwikkeling** van het **federatief datastelsel** van **Nederland** staat hier centraal,
waarbij het programma [Realisatie IBDS](https://realisatieibds.pleio.nl/) het belangrijk vindt deze
ontwikkeling samen met de omgeving te realiseren! Op dit moment ontwikkelt het programmaonderdeel
[Realisatie Federatief
Datastelsel](https://realisatieibds.pleio.nl/cms/view/8852ee2a-a28a-4b91-9f3e-aab229bbe07f/federatief-datastelsel)
(R-FDS) de basismogelijkheden die de interbestuurlijke datastrategie mogelijk maken.

Op deze site is de laatste versie te vinden van het federatief datastelsel. Deze is niet alleen te
lezen en te bekijken maar ook door iedereen van commentaar en vragen te voorzien. Dit is een
vereiste om met elkaar het federatief datastelsel in Nederland open en toegankelijk te ontwikkelen.
Lees meer hierover in onze [strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/) of
duik direct in onze [werkomgeving](/docs/welkom/werkomgeving/). Die laat je precies zien waar en hoe
dit kan!

{{% /blocks/lead %}}


{{% blocks/section color="dark" type="row" %}}
{{% blocks/feature icon="fa-comments" title="Mattermost" %}}

We begrijpen dat je vragen hebt over de ontwikkeling van het federatief datastelsel, kom daarom
rechtstreeks met ons in contact.

Via deze chat op [digilab.overheid.nl/chat](https://digilab.overheid.nl/chat) kun je met alle
betrokken in contact komen!

{{% /blocks/feature %}}


{{% blocks/feature icon="fab fa-gitlab" title="Contributies zijn welkom" url="/docs/community/contribution/" %}}

We nodigen je uit mee te bouwen aan het stelsel, ga naar GitLab en dien je aanvullingen in.

Meer weten over hoe?

{{% /blocks/feature %}}


{{% blocks/feature icon="fab fa-linkedin" title="Volg ons" %}}

Wil je vooral nu up to date blijven over de nieuwste ontwikkelingen, volg Realisatie IBDS dan op
[LinkedIn](https://www.linkedin.com/company/realisatie-ibds/)

{{% /blocks/feature %}}


{{% /blocks/section %}}
